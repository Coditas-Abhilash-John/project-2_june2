const questionEl = document.getElementById("question-section") as HTMLElement;
const optionEl = document.getElementById("option-section") as HTMLElement;
const dropDownEl = document.getElementById("dropdown-btn") as HTMLButtonElement;
const checkBoxEl = document.getElementById("checkbox-btn") as HTMLButtonElement;
const textBoxEl = document.getElementById("text-btn") as HTMLButtonElement;
const numericEl = document.getElementById("numeric-btn") as HTMLButtonElement;
const submitEl = document.getElementById("submit-btn") as HTMLButtonElement;

